<?php
    /**
     * R
     * @version 1.00
     * @copyright (c) 2013, Niko Hujanen
     */
    require_once 'R/R.php';
    
    R::get('test/%d/%s', function($id, $token) {
        
        echo '<pre>';
        var_dump( $id );
        var_dump( $token );
        
    });
    
    R::run();