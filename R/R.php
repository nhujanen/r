<?php
final class R
{
    static $uri     = false;
    static $url     = false;
    static $base    = false;
    static $routes  = array();
    
    public static function run()
    {
        self::$uri  = trim(substr($_SERVER['REQUEST_URI'], strlen(dirname($_SERVER['SCRIPT_NAME']))), '/');
        self::$base = ($_SERVER['SERVER_PORT'] == 443) ? 'https' : 'http';
        self::$base .= '://';
        self::$base .= $_SERVER['SERVER_NAME'];
        self::$base .= str_replace(self::$uri, '', $_SERVER['REQUEST_URI']);
        self::$url  = self::$base . self::$uri;
        
        $_method = strtoupper($_SERVER['REQUEST_METHOD']);
        if (is_array(self::$routes[$_method])) {
            foreach (self::$routes[$_method] as $_route => $_callback) {
                $_result = array_filter(sscanf(self::$uri, $_route));
                $_reflection = new ReflectionFunction($_callback);
                
                if (count($_result) !== count($_reflection->getParameters())) continue;
                
                $_parameters = array();
                foreach ($_reflection->getParameters() as $_parameter) {
                    $_parameters[$_parameter->name] = array_shift($_result);
                }

                return call_user_func_array($_callback, $_parameters);
            }
        }
        
        header('x', true, 404);
        exit;
    }
    
    public static function get($uri, $callback)
    {
        return (self::$routes['GET'][$uri] = $callback);
    }
    
    public static function post($uri, $callback)
    {
        return (self::$routes['POST'][$uri] = $callback);
    }
    
    public static function put($uri, $callback)
    {
        return (self::$routes['PUT'][$uri] = $callback);
    }
    
    public static function delete($uri, $callback)
    {
        return (self::$routes['DELETE'][$uri] = $callback);
    }
}